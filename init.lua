local vim = vim

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- For workies on nixos
-- require("lazy").lockfile = vim.fn.stdpath("data") .. "/lazy-lock.json";

require("lazy").setup({
  { "neovim/nvim-lspconfig",             lazy = true },
  { "williamboman/mason.nvim",           lazy = true },
  { "williamboman/mason-lspconfig.nvim", lazy = true },
  { "hrsh7th/nvim-cmp",                  lazy = true },
  { "hrsh7th/cmp-nvim-lsp",              lazy = true },
  "hrsh7th/cmp-vsnip",
  "hrsh7th/vim-vsnip",
  "hrsh7th/cmp-nvim-lsp-signature-help",
  {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.6',
    dependencies = { 'nvim-lua/plenary.nvim' },
  },
  -- "L3MON4D3/LuaSnip",
  { "folke/tokyonight.nvim", lazy = true },
  { "rose-pine/neovim",      lazy = true },
  {
    "folke/which-key.nvim",
    event = "VeryLazy",
    init = function()
      vim.o.timeout = true
      vim.o.timeoutlen = 300
    end,
    opts = {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
  },
  {
    'nvim-lualine/lualine.nvim',
    dependencies = { 'nvim-tree/nvim-web-devicons' }
  },
  { "gelguy/wilder.nvim",    lazy = true },
  "JoosepAlviste/nvim-ts-context-commentstring",
  { "numToStr/Comment.nvim", lazy = true },
  {
    "jiaoshijie/undotree",
    dependencies = "nvim-lua/plenary.nvim",
    lazy = true
  },
  "aznhe21/actions-preview.nvim"
},
{
  lockfile = vim.fn.stdpath("data") .. "/lazy-lock.json";
})

require('undotree').setup()
require('Comment').setup()

local wilder = require("wilder")

wilder.setup({ modes = { ":", "/", "?" } })
wilder.set_option('renderer', wilder.popupmenu_renderer(
  wilder.popupmenu_border_theme({
    highlights = {
      border = 'Normal', -- highlight to use for the border
    },
    left = { ' ', wilder.popupmenu_devicons() },
    -- 'single', 'double', 'rounded' or 'solid'
    -- can also be a list of 8 characters, see :h wilder#popupmenu_border_theme() for more details
    border = 'rounded',
  })
))

-- note: diagnostics are not exclusive to lsp servers
-- so these can be global keybindings
vim.keymap.set('n', 'gl', '<cmd>lua vim.diagnostic.open_float()<cr>')
vim.keymap.set('n', '[d', '<cmd>lua vim.diagnostic.goto_prev()<cr>')
vim.keymap.set('n', ']d', '<cmd>lua vim.diagnostic.goto_next()<cr>')

vim.api.nvim_create_autocmd('LspAttach', {
  desc = 'LSP actions',
  callback = function(event)
    local opts = { buffer = event.buf }

    -- these will be buffer-local keybindings
    -- because they only work if you have an active language server

    vim.keymap.set('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
    vim.keymap.set('n', 'gd', '<cmd>Telescope lsp_definitions<cr>', opts)
    vim.keymap.set('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<cr>', opts)
    vim.keymap.set('n', 'gi', '<cmd>Telescope lsp_implementations<cr>', opts)
    vim.keymap.set('n', 'go', '<cmd>Telescope lsp_type_definitions<cr>', opts)
    vim.keymap.set('n', 'gr', '<cmd>Telescope lsp_references<cr>', opts)
    vim.keymap.set('n', 'gs', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
    vim.keymap.set('n', '<leader>r', '<cmd>lua vim.lsp.buf.rename()<cr>', opts)
    vim.keymap.set({ 'n', 'x' }, '<F3>', '<cmd>lua vim.lsp.buf.format({async = true})<cr>', opts)
    -- vim.keymap.set('n', '<leader>a', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)
    vim.keymap.set("n", "<leader>a", require("actions-preview").code_actions, opts)
    vim.keymap.set("n", "gh", "<cmd>ClangdSwitchSourceHeader<cr>", opts)
  end
})

vim.g.skip_ts_context_commentstring_module = true

vim.cmd([[
augroup highlight_yank
    autocmd!
    au TextYankPost * silent! lua vim.highlight.on_yank{higroup="IncSearch", timeout=100}
augroup END
]])

vim.g.mapleader = " "

vim.keymap.set('n', '<leader>u', '<cmd>lua require("undotree").toggle()<cr>')
vim.keymap.set("n", "<leader>f", "<cmd>Telescope fd<cr>")
vim.keymap.set("n", "<leader>D", "<cmd>Telescope diagnostics<cr>")
vim.keymap.set("n", "<leader>b", "<cmd>Telescope buffers<cr>")
vim.keymap.set("n", "<leader>s", "<cmd>Telescope lsp_document_symbols<cr>")

vim.keymap.set("n", "<leader>w", "<C-W>")

vim.opt.ignorecase = true
vim.opt.smartcase = true
vim.opt.cmdheight = 1
vim.opt.smarttab = true
vim.opt.expandtab = true
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2

vim.opt.number = true
vim.opt.relativenumber = true
vim.opt.nuw = 1
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"

vim.opt.termguicolors = true

vim.opt.undodir = vim.fn.expand("~/.vim/undodir")
vim.opt.undofile = true;

vim.cmd(":colorscheme rose-pine-moon")

require("lualine-config")

local lsp_capabilities = require('cmp_nvim_lsp').default_capabilities()

local default_setup = function(server)
  require('lspconfig')[server].setup({
    capabilities = lsp_capabilities,
  })
end

require('mason').setup({})
require('mason-lspconfig').setup({
  ensure_installed = {},
  handlers = {
    default_setup,
  },
})

require("mason-lspconfig").setup_handlers({
  -- First entry is default handler 
  function (server_name)
    require("lspconfig")[server_name].setup({})
  end,

  ["clangd"] = function()
    require("lspconfig").clangd.setup({
      cmd = {
        "clangd",
        "--background-index",
        "--suggest-missing-includes"
      }
    })
  end
})

local cmp = require('cmp')

-- vim.keymap.set("n", "<C-Space>", cmp.mapping.complete())

cmp.setup({
  sources = {
    { name = 'nvim_lsp' },
    { name = "vsnip" },
    { name = 'nvim_lsp_signature_help' }
  },
  window = {
    documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    -- Enter key confirms completion item
    ['<CR>'] = cmp.mapping.confirm({ select = true }),

    ['<Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      else
        fallback()
      end
    end,

    ['<S-Tab>'] = function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      else
        fallback()
      end
    end,

    -- Ctrl + space triggers completion menu
    ['<C-Space>'] = cmp.mapping.complete(),
  }),
  snippet = {
    expand = function(args)
      vim.cmd("echo 'Expanding " .. args.body .. "'")
      -- require('luasnip').lsp_expand(args.body)
      -- vim.snippet.expand(args.body)
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
})

-- Make docs look better
vim.lsp.util.stylize_markdown = function(bufnr, contents, opts)
  contents = vim.lsp.util._normalize_markdown(contents, {
    width = vim.lsp.util._make_floating_popup_size(contents, opts),
  })

  vim.bo[bufnr].filetype = "markdown"
  vim.treesitter.start(bufnr)
  vim.api.nvim_buf_set_lines(bufnr, 0, -1, false, contents)

  return contents
end
